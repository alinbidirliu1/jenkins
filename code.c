#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <setjmp.h>

#include "Pipeline_code_C.h"


// the function to test:/
int my_div(int x, int y);

int main()
{
    printf("Result of div 2/1: %d\n", my_div(2,1));
    return 1;
}
